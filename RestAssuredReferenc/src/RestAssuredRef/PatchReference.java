package RestAssuredRef;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class PatchReference {

	public static void main(String[] args) {
		
		// Step 1 : Declare the variable for Base URI and RequestBody
		String BaseURI = "https://reqres.in/";
		String RequestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		
		// Step 2 : Create an object of JsonPath to parse the RequestBody
		JsonPath jsp_req = new JsonPath(RequestBody);

		// Step 3 : Extract Values From RequestBody
		String req_name = jsp_req.getString("name");
		System.out.println("RequestBody Parameter 1 :" + req_name);
		String req_job = jsp_req.getString("job");
		System.out.println("RequestBody Parameter 2 :" + req_job);

		// Step 4 : Declare BaseURI
		RestAssured.baseURI = BaseURI;

		// Step 5 : Configure RequestBody and trigger the RequestBody
		/*
		 * String ResponseBody=given().header("Content-Type","application/json").body(
		 * RequestBody).when().put("api/users/2")
		 * .then().extract().response().asString(); System.out.println(ResponseBody);
		 */

		String ResponseBody = given().header("Content-Type", "application/json").body(RequestBody).when()
				.put("api/users/2").then().extract().response().asString();

		// Step 6 : Create an object of JsonPath to parse the ResponseBody
		JsonPath jsp_res = new JsonPath(ResponseBody);

		// Step 7 : Extract the values from ResponseBody
		String res_name = jsp_res.getString("name");
		System.out.println("ResponseBody Parameter 1 :" + res_name);

		String res_job = jsp_res.getString("job");
		System.out.println("ResponseBody Parameter 2 :" + res_job);
		
		// Step 8 : Validate the ResponseBody Parameters
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);

	}

}
