package RestAssuredRef;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Get1Ref {

    public static void main(String[] args) {

        // Step 1 : Declare the variable for BaseURI
        String BaseURI = "https://reqres.in/";

        // Step 2 : Declare BaseURI
        RestAssured.baseURI = BaseURI;

        // Step 3 : Make a GET request and fetch the response body
        String ResponseBody = given().header("Content-Type", "application/json").when().get("api/users?page=2").then()
                .extract().response().asString();

        // Step 4 : Create an object of JsonPath to parse the response body
        JsonPath jsp_res = new JsonPath(ResponseBody);

        // Step 5 : Extract values from request body
        int PerPage = jsp_res.getInt("per_page"); // Get the actual per_page value

        // Fetch expected data dynamically
        JsonPath jsp_expected = given().header("Content-Type", "application/json").when().get("api/expected_data").then()
                .extract().jsonPath(); // Replace "api/expected_data" with the actual API endpoint to fetch expected data
        
        System.out.println("Expected Data Response Body: " + ResponseBody);
       /* System.out.println("Expected ID values: " + jsp_expected.get("expected_id"));
        System.out.println("Expected Email values: " + jsp_expected.get("expected_email"));
        System.out.println("Expected Email values: " + jsp_expected.get("expected_first_name"));
        System.out.println("Expected Email values: " + jsp_expected.get("expected_last_name"));
        System.out.println("Expected Email values: " + jsp_expected.get("expected_avatar"));*/
        
        int[] req_id = jsp_expected.get("expected_id");
        String[] req_email = jsp_expected.get("expected_email");
        String[] req_first_name = jsp_expected.get("expected_first_name");
        String[] req_last_name = jsp_expected.get("expected_last_name");
        String[] req_avatar = jsp_expected.get("expected_avatar");

        // Step 6 : Validate individual data objects
        for (int i = 0; i < PerPage; i++) {
            int actualId = jsp_res.getInt("data[" + i + "].id");
            String actualEmail = jsp_res.getString("data[" + i + "].email");
            String actualFirstName = jsp_res.getString("data[" + i + "].first_name");
            String actualLastName = jsp_res.getString("data[" + i + "].last_name");
            String actualAvatar = jsp_res.getString("data[" + i + "].avatar");

            // Step 7 : Your expected values
            int expectedId = req_id[i];
            String expectedEmail = req_email[i];
            String expectedFirstName = req_first_name[i];
            String expectedLastName = req_last_name[i];
            String expectedAvatar = req_avatar[i];

            // Step 8 : Validate the response body parameters
            Assert.assertEquals(actualId, expectedId);
            Assert.assertEquals(actualEmail, expectedEmail);
            Assert.assertEquals(actualFirstName, expectedFirstName);
            Assert.assertEquals(actualLastName, expectedLastName);
            Assert.assertEquals(actualAvatar, expectedAvatar);

            // Step 9 : Print validation status
            System.out.println("Validation successful for data entry " + i);
        }
    }
}