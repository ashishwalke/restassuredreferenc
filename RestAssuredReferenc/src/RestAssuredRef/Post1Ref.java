package RestAssuredRef;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Post1Ref {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// declare the variable for base uri and RequestBody

		String BaseURI = "https://reqres.in/";
		String RequestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

		// declare BaseURI
		RestAssured.baseURI = BaseURI;

		// configure the request body and trigger the response

		String ResponseBody = given().header("Content-Type", "application/json").body(RequestBody).when()
				.post("api/users").then().extract().response().asString();
		System.out.println(ResponseBody);

		// fetch or extract the requestBody using json path

		JsonPath jsp_req = new JsonPath(RequestBody);

		String req_name = jsp_req.getString("name");
		System.out.println("The requestBody parameter is : " + req_name);

		String req_job = jsp_req.getString("job");
		System.out.println("the requestBody parameter is : " + req_job);

		// fetch or extract response Body using json path

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");
		System.out.println("the response Body parameter are : " + res_name);

		String res_job = jsp_res.getString("job");
		System.out.println("The response Body parameter are" + res_job);

		String res_id = jsp_res.getString("id");
		System.out.println("The response Body parameter are " + res_id);

		String res_createdAt = jsp_res.getString("createdAt");
		System.out.println("The response Body parameter are : " + res_createdAt);

		// Validate the response Body

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);

	}

}
