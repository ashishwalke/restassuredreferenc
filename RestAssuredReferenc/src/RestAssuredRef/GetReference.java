package RestAssuredRef;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class GetReference {

	public static void main(String[] args) {

		// Step 1 : Declare the variable for BaseURI
		String BaseURI = "https://reqres.in/";

		// Step 2 : Declare BaseURI
		RestAssured.baseURI = BaseURI;

		// Step 3 : Make a GET request and fetch the response body
		String ResponseBody = given().header("Content-Type", "application/json").when().get("api/users?page=2").then()
				.extract().response().asString();

		// Step 4 : Extract values from request body
		int[] req_id = { 7, 8, 9, 10, 11, 12 };
		String[] req_email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] req_first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] req_last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] req_avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		// Step 5 : Create an object of JsonPath to parse the response body
		JsonPath jsp_res = new JsonPath(ResponseBody);
		int PerPage = jsp_res.getInt("per_page"); // Get the actual per_page value

		// Step 6 : Validate individual data objects
		for (int i = 0; i < PerPage; i++) {
			// Extract actual values from the parsed JSON response
			int actualId = jsp_res.getInt("data[" + i + "].id");
			String actualEmail = jsp_res.getString("data[" + i + "].email");
			String actualFirstName = jsp_res.getString("data[" + i + "].first_name");
			String actualLastName = jsp_res.getString("data[" + i + "].last_name");
			String actualAvatar = jsp_res.getString("data[" + i + "].avatar");

			// Step 7 : Your expected values
			int expectedId = req_id[i];
			String expectedEmail = req_email[i];
			String expectedFirstName = req_first_name[i];
			String expectedLastName = req_last_name[i];
			String expectedAvatar = req_avatar[i];

			// Step 8 : Validate the response body parameters
			Assert.assertEquals(actualId, expectedId);
			Assert.assertEquals(actualEmail, expectedEmail);
			Assert.assertEquals(actualFirstName, expectedFirstName);
			Assert.assertEquals(actualLastName, expectedLastName);
			Assert.assertEquals(actualAvatar, expectedAvatar);

			// Step 9 : Print validation status
			System.out.println("Validation successful for data entry " + i);
		}
	}
}